package sbu.cs.multithread.priority;


import java.util.concurrent.CountDownLatch;

public class BlueThread extends ColorThread {
    private static final String MESSAGE = "hi back blacks, hi whites";
    private CountDownLatch latch;

    public BlueThread(CountDownLatch latch) {
        this.latch = latch;
    }

    void printMessage() {
        super.printMessage(new Message(this.getClass().getName(), getMessage()));
    }

    @Override
    String getMessage() {
        return MESSAGE;
    }

    @Override
    public void run() {
        printMessage();
        latch.countDown();
    }
}
