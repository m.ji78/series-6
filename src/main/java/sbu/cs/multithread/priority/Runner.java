package sbu.cs.multithread.priority;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

public class Runner {

    public static List<Message> messages = new ArrayList<>();

    /**
     * add your codes to this function. this function is the caller function which will be called first.
     * changing other codes in this function is allowed.
     *
     * @param blackCount    number of black threads
     * @param blueCount     number of blue threads
     * @param whiteCount    number of white threads
     */
    public void run(int blackCount, int blueCount, int whiteCount) throws InterruptedException {
        CountDownLatch blackLatch = new CountDownLatch(blackCount);
        CountDownLatch blueLatch = new CountDownLatch(blueCount);
        CountDownLatch whiteLatch = new CountDownLatch(whiteCount);

        BlackThread[] blackThreads = new BlackThread[blackCount];
        BlueThread[] blueThreads = new BlueThread[blueCount];
        WhiteThread[] whiteThreads = new WhiteThread[whiteCount];


        for (BlackThread b : blackThreads) {
            b = new BlackThread(blackLatch);
            b.start();
        }

        blackLatch.await();

        for (BlueThread b : blueThreads) {
            b = new BlueThread(blueLatch);
            b.start();
        }

        blueLatch.await();

        for (WhiteThread b : whiteThreads) {
            b = new WhiteThread(whiteLatch);
            b.start();
        }

        whiteLatch.await();
    }

    synchronized public static void addToList(Message message) {
        messages.add(message);
    }

    public List<Message> getMessages() {
        return messages;
    }

    /**
     *
     * @param args
     */
    public static void main(String[] args) {

    }
}
