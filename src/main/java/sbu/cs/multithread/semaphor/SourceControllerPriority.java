package sbu.cs.multithread.semaphor;

import java.util.concurrent.Semaphore;

public class SourceControllerPriority {

    public static void main(String[] args) throws InterruptedException {
        Semaphore semaphore = new Semaphore(2);


        Chef chef1 = new Chef("chef1", semaphore);
        Chef chef2 = new Chef("chef2", semaphore);
        Chef chef3 = new Chef("chef3", semaphore);
        Chef chef4 = new Chef("chef4", semaphore);
        Chef chef5 = new Chef("chef5", semaphore);

        chef1.setPriority(Thread.MAX_PRIORITY);
        chef2.setPriority(Thread.NORM_PRIORITY);
        chef3.setPriority(Thread.MAX_PRIORITY);
        chef4.setPriority(Thread.MIN_PRIORITY);
        chef5.setPriority(Thread.MAX_PRIORITY);

        chef1.start();
        chef3.start();
        chef5.start();
        chef2.start();
        chef4.start();
    }
}
