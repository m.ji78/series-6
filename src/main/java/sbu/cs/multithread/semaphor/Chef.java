package sbu.cs.multithread.semaphor;


import java.util.concurrent.Semaphore;

public class Chef extends Thread {
    private final Semaphore semaphore;
    private final String name;

    public Chef(String name, Semaphore semaphore) {
        this.name = name;
        this.semaphore = semaphore;
    }

    @Override
    public void run() {
        Source source = new Source(semaphore);
        for (int i = 0; i < 10; i++) {
            try {
                source.getSource(name);

                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
