package sbu.cs.multithread.semaphor;

import java.util.concurrent.Semaphore;

public class Source {
    private final Semaphore semaphore;

    public Source(Semaphore semaphore) {
        this.semaphore = semaphore;
    }

    public void getSource(String name) throws InterruptedException {
        try {
            semaphore.acquire();
            Thread.sleep(100);

            long id = Thread.currentThread().getId();
            long priority = Thread.currentThread().getPriority();

            System.out.println("name: " + name + ",  id: " + id + ",  priority: " + priority + " started ");

            //Do anything
            Thread.sleep(1500);

            System.out.println("name: " + name + ",  id: " + id + ",  priority: " + priority + " finished ");

            Thread.sleep(100);
            semaphore.release();

            System.out.println();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
