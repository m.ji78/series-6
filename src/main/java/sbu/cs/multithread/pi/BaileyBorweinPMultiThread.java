package sbu.cs.multithread.pi;

import org.jscience.mathematics.number.Real;

import java.util.concurrent.Callable;

public class BaileyBorweinPMultiThread implements Callable<Real> {
    private int index;
    private Real pi = Real.ZERO;

    public BaileyBorweinPMultiThread(int index) {
        this.index = index;

    }

    @Override
    public Real call() throws Exception {
        Real.setExactPrecision(1005);
        Real sum = Real.ZERO;
        Real fraction1 = divide(4, 8 * index + 1);
        Real fraction2 = divide(-2, 8 * index + 4);
        Real fraction3 = divide(-1, 8 * index + 5);
        Real fraction4 = divide(-1, 8 * index + 6);
        Real fraction5 = Real.valueOf(16);

        if (index == 0) {
            fraction5 = Real.ONE;
        } else {
            fraction5 = fraction5.pow(index);
            fraction5 = Real.ONE.divide(fraction5);
        }

        sum = sum.plus(fraction1).plus(fraction2).plus(fraction3).plus(fraction4);
        sum = sum.times(fraction5);
        pi = pi.plus(sum);

        return pi;
    }


    public Real divide(int numerator, int denominator) {
        Real result = Real.valueOf(numerator);
        return result.divide(denominator);
    }
}
