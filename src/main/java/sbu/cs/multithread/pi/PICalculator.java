package sbu.cs.multithread.pi;

import org.jscience.mathematics.number.Real;


import java.util.concurrent.*;

public class PICalculator {

    /**
     * calculate pi and represent it as string with given floating point number (numbers after .)
     * check test cases for more info
     * check pi with 1000 digits after floating point at https://mathshistory.st-andrews.ac.uk/HistTopics/1000_places/
     *
     * @param floatingPoint number of digits after floating point
     * @return pi in string format
     */
    public String calculate(int floatingPoint) throws ExecutionException, InterruptedException {

        ExecutorService pool = Executors.newFixedThreadPool(10);
        Callable<Real> callable;
        Future<Real> future;

        Real sum = Real.ZERO;
        for (int i = 0; i < 1000; i++) {
            callable = new BaileyBorweinPMultiThread(i);
            future = pool.submit(callable);
            sum = sum.plus(future.get());
        }
        pool.shutdown();

        return String.valueOf(sum).substring(0, floatingPoint + 2);
    }
}
