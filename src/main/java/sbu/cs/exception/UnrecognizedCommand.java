package sbu.cs.exception;

public class UnrecognizedCommand extends ApException {

    public UnrecognizedCommand() {
        super("This Command is not recognized");
    }
}
