package sbu.cs.exception;

public class BadInput extends ApException {

    public BadInput() {
        super("This is a wrong input");
    }

}
