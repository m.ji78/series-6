package sbu.cs.exception;

public class ApException extends Exception {
    private String message;

    public ApException(String message) {
        super(message);
        this.message = message;
    }
}
