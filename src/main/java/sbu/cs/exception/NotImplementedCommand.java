package sbu.cs.exception;

public class NotImplementedCommand extends ApException {

    public NotImplementedCommand() {
        super("This Command is not implemented");
    }

}
